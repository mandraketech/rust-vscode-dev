FROM rust:1-slim

ARG USERNAME

RUN --mount=type=cache,target=/var/cache/apt \
  # update apt-cache \
  apt-get update && \
  apt-get --no-install-recommends install -y wget curl tar zip unzip ca-certificates git openssh-client && \
  apt-get clean -y && \
  apt-get autoremove -y

RUN USER_ID=9001 && GROUP_ID=${USER_ID} && \
    addgroup --gid ${GROUP_ID} ${USERNAME} && \
    adduser --uid ${USER_ID} --gid ${GROUP_ID} --shell /bin/bash --disabled-password ${USERNAME}

RUN mkdir /workspaces && \
    chown -R ${USERNAME}:${USERNAME} /workspaces

VOLUME [ "/workspaces" ]

USER ${USERNAME}

SHELL [ "/bin/bash", "-c" ]

RUN rustup component add clippy rustfmt rust-docs

ENV PATH=${HOME}/.cargo/bin:${PATH}

WORKDIR /workspaces

CMD ["sleep", "infinity"]